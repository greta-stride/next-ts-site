// import type { NextPage } from "next"

// const Home: NextPage = () => {
//   return (
//     <div className="centered-page">
//       <style global jsx>{`
//         .title {
//           color: red;
//           font-size: 40px;
//         }

//         .centered-page {
//           min-height: 100vh;
//           display: flex;
//           flex-direction: column;
//           justify-content: center;
//           align-items: center;
//         }

//         @media (max-width: 600px) {
//           .title {
//             color: purple;
//           }
//         }
//       `}</style>

//       <h1 className="text-4xl text-blue-600 font-bold underline">
//         FrontEnd Gadget
//       </h1>
//       <button className="btn btn-outline-primary">VIEW CATALOG</button>
//       <img src=""></img>
//     </div>
//   )
// }

// export default Home

import type { NextPage } from "next"
import React from "react"
import Image from "next/image"
import image from "/public/images/react.jpg"
import Head from "next/head"

const Home: NextPage = () => {
  return (
    <>
      <Head>
        <title>FrontEnd Gadgets</title>
        <meta
          name="description"
          content="Front-end Gadgets is a demo site to learn Next"
        />
        <link rel="icon" href="/favicon.ico" />
      </Head>
      <Image src={image} alt="React logo" />
      <h1 className="title">FrontEnd Gadgets</h1>
      <button className="button">VIEW CATALOG</button>
    </>
  )
}

export default Home
