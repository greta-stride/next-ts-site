import { NextPage } from "next"
import Link from "next/link"

const Page404: NextPage = () => {
  return (
    <div>
      <h1 className="title">Pagina non trovata</h1>
      <Link href="/" legacyBehavior>
        <a className="text-pink-500 ">Torna alla home</a>
      </Link>
    </div>
  )
}

export default Page404
