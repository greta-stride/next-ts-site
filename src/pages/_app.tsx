import Footer from "@/components/Footer"
import { Navbar } from "@/components/Navbar"
import "@/styles/globals.css"
import "animate.css/animate.min.css"
import type { AppProps } from "next/app"

export default function App({ Component, pageProps }: AppProps) {
  return (
    <div className="page">
      <Navbar />
      <Component {...pageProps} />
      <Footer />
    </div>
  )
}
